#include "cleanupdialog.h"
#include "ui_cleanupdialog.h"
#include <QListWidgetItem>

CleanupDialog::CleanupDialog(QList<QPair<QWidget*, QString> > outdatedTabs, QWidget *parent) :
    QDialog(parent),
    m_outdatedTabs(outdatedTabs),
    ui(new Ui::CleanupDialog)
{
    ui->setupUi(this);
    int count = m_outdatedTabs.count();
    for(int i = 0; i<count; i++){
        QString ts = m_outdatedTabs.at(i).second;
        QListWidgetItem *listItem = new QListWidgetItem(ts);
        listItem->setFlags(listItem->flags() | Qt::ItemIsUserCheckable);
        listItem->setCheckState(Qt::Checked);
        ui->klw_oudatedTabs->addItem(listItem);
    }

}

QList<QWidget*> CleanupDialog::checkedTabs()
{
    QList<QWidget*> selectedTabs;

    int countItems = ui->klw_oudatedTabs->count();
    for (int i = 0; i < countItems; ++i) {
        if (ui->klw_oudatedTabs->item(i)->checkState() == Qt::Checked) {
            selectedTabs.append(this->m_outdatedTabs.at(i).first);
        }
    }

    return selectedTabs;
}

CleanupDialog::~CleanupDialog()
{
    delete ui;
}
