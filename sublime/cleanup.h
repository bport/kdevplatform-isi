/***************************************************************************
 *   Copyright 2006-2007 Yannick Motta   <yannick.motta@gmail.com>         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#ifndef CLEANUP_H
#define CLEANUP_H
#include <QObject>
#include <QMap>
#include <QPair>
#include <QTimer>

class QWidget;
class QSignalMapper;
class KTabBar;
class QStackedWidget;

namespace Sublime {
    class Container;
}

class ICleanupStrategy
{
    public:
        virtual void initCleanup(Sublime::Container *c, KTabBar *tabBar, QStackedWidget *stack) = 0;
        virtual QList<QPair<QWidget*, QString> > outdatedDocumentsList() = 0;
        virtual void setOutdatedTime(int) = 0;
};

class WidgetTimer : public QTimer
{
    Q_OBJECT
public:
    WidgetTimer(QString name, QWidget* widget, QObject* parent);
    WidgetTimer(int index, QObject* parent);
signals:
    void timeout(QWidget* widget, QString name);
    void timeout(int index);
private slots:
    void reemitWidgetTimeout();
    void reemitIndexTimeout();
private:
    QWidget* m_widget;
    QString m_name;
    int m_index;
};

class SmartCleanup : QObject, ICleanupStrategy
{
    Q_OBJECT
    public:
        void initCleanup(Sublime::Container *c, KTabBar *tabBar, QStackedWidget *stack);
        QList<QPair<QWidget*, QString> > outdatedDocumentsList();
        void removeOutdated(QWidget* w);
        void setOutdatedTime(int time);
    public slots:
        void currentChanged(int index);
        void currentChangedDelayed(int index);
        void updateOutdated(QWidget* widget, QString name);
        void addTimer(QWidget*);
        void deleteTimer(QWidget*);
        void deleteDelayedTimer();
        void closeRequest(int index);
    private:
        Sublime::Container* m_container;
        KTabBar* m_tabBar;
        QStackedWidget* m_stack;
        QList<QPair<QWidget*, QString> > m_outdatedTabs;
        QMap<QWidget*, WidgetTimer*> m_timers;
        QSignalMapper* m_signalMapper;
        QWidget* m_current;
        QList<QWidget*> m_lastDocuments;
        WidgetTimer* m_delayedTimer;
        int m_count;
        int s_outdatedTime;
};

#endif // CLEANUP_H
