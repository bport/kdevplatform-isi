/***************************************************************************
 *   Copyright 2006-2007 Yannick Motta   <yannick.motta@gmail.com>         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/
#include "cleanup.h"
#include <QSignalMapper>
#include <KTabBar>
#include <QStackedWidget>
#include <KGlobal>
#include <KConfigGroup>
#include "container.h"

void SmartCleanup::initCleanup(Sublime::Container* c, KTabBar *tabBar, QStackedWidget *stack)
{
    m_container = c;
    m_tabBar = tabBar;
    m_stack = stack;

    connect(m_tabBar, SIGNAL(currentChanged(int)), this, SLOT(currentChangedDelayed(int)));
    connect(m_tabBar, SIGNAL(closeRequest(int)), this, SLOT(closeRequest(int)));
    connect(m_container, SIGNAL(widgetAdded(QWidget*)), this, SLOT(addTimer(QWidget*)));

    //Add timer for all tabs
    int count = m_stack->count();
    for (int i=0; i < count; i++){
        addTimer(m_stack->widget(i));
    }


    //remove timer for current tab
    delete m_timers[m_stack->currentWidget()];
    m_timers.remove(m_stack->currentWidget());
    m_current = m_stack->currentWidget();
    removeOutdated(m_current);
    m_count = 0;

}

void SmartCleanup::updateOutdated(QWidget* widget, QString name)
{
    if (widget != m_current){
        //manage duplicate
        int count = m_outdatedTabs.count();
        bool duplicate = 0;
        for(int i = 0; i<count && !duplicate; i++){
            duplicate = m_outdatedTabs.at(i).first == widget;
        }
        if(!duplicate){
            m_outdatedTabs.append(qMakePair(widget, name));
        }
        deleteTimer(widget);
    }
}

void SmartCleanup::currentChanged(int index)
{
    deleteDelayedTimer();
    //delete timer on new current tab
    deleteTimer(m_stack->widget(index));
    //add timer on old current
    addTimer(m_current);
    m_current = m_stack->widget(index);
    //remove current from outdated list
    removeOutdated(m_current);
    //add current to last documents list
    m_lastDocuments.append(m_current);
    if (m_lastDocuments.size() > 5){
        m_lastDocuments.removeFirst();
    }

}

void SmartCleanup::currentChangedDelayed(int index)
{
    deleteDelayedTimer();
    if (m_count > 0){
        if (m_current != m_stack->currentWidget()){
            m_delayedTimer = new WidgetTimer(index, this);
            m_delayedTimer->start(5000);
            connect(m_delayedTimer, SIGNAL(timeout(int)), this, SLOT(currentChanged(int)));
        }
    }
    else {
        m_count++;
        currentChanged(index);
    }
}

void SmartCleanup::deleteDelayedTimer()
{
    if (m_delayedTimer){
        disconnect(m_delayedTimer, SIGNAL(timeout(int)), this, SLOT(currentChanged(int)));
        delete m_delayedTimer;
        m_delayedTimer = 0;
    }
}

void SmartCleanup::closeRequest(int index)
{
    removeOutdated(m_stack->widget(index));
}

void SmartCleanup::removeOutdated(QWidget* w){
    //delete from outdated list
    int count = m_outdatedTabs.count();
    bool duplicate = 0;
    for(int i = 0; i<count && !duplicate; i++){
        duplicate = m_outdatedTabs.at(i).first ==  w;
        if(duplicate){
            m_outdatedTabs.removeAt(i);
        }
    }
}


void SmartCleanup::addTimer(QWidget * w){
    if (w){
        WidgetTimer* timer = new WidgetTimer(m_tabBar->tabText(m_stack->indexOf(w)), w, this);
        m_timers[w] = timer;
        timer->start(SmartCleanup::s_outdatedTime);
        connect(timer, SIGNAL(timeout(QWidget*, QString)), this, SLOT(updateOutdated(QWidget*, QString)));
     }
}

void SmartCleanup::deleteTimer(QWidget * w){
    if (w){
       delete m_timers[w];
       m_timers.remove(w);
    }
}

void SmartCleanup::setOutdatedTime(int time){
    SmartCleanup::s_outdatedTime = time*60000;
}

QList<QPair<QWidget*, QString> > SmartCleanup::outdatedDocumentsList(){

    QList<QPair<QWidget*, QString> > outdatedList;

    int count = m_outdatedTabs.count();
    bool inLastDocuments = false;

    for(int i = 0; i<count; i++){
        foreach (QWidget* w, m_lastDocuments){
                inLastDocuments |= m_outdatedTabs.at(i).first == w;
        }
        if(!inLastDocuments){
            outdatedList.append(m_outdatedTabs.at(i));
        }
        inLastDocuments = false;
    }
    return m_outdatedTabs;
}

//class WidgetTimer

WidgetTimer::WidgetTimer(QString name,QWidget *widget, QObject *parent) : QTimer(parent){
    m_widget = widget;
    m_name = name;
    connect(this, SIGNAL(timeout()), this, SLOT(reemitWidgetTimeout()));
}

WidgetTimer::WidgetTimer(int index, QObject *parent) : QTimer(parent){
    m_index = index;
    connect(this, SIGNAL(timeout()), this, SLOT(reemitIndexTimeout()));
}

void WidgetTimer::reemitWidgetTimeout(){
    emit timeout(m_widget, m_name);
}

void WidgetTimer::reemitIndexTimeout(){
    emit timeout(m_index);
}

#include "cleanup.moc"
