#ifndef CLEANUPDIALOG_H
#define CLEANUPDIALOG_H

#include <QDialog>

namespace Ui {
    class CleanupDialog;
}

class CleanupDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CleanupDialog(QList<QPair<QWidget*, QString> > outdatedTabs, QWidget *parent = 0);
    ~CleanupDialog();

    QList<QWidget*> checkedTabs();

private:
    Ui::CleanupDialog *ui;
    QList<QPair<QWidget*, QString> > m_outdatedTabs;
};

#endif // CLEANUPDIALOG_H
