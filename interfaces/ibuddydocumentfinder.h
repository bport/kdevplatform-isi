/*
    Plugin extension interface to find an open buddy document,
    may be implemented e.g. by language support plugins.
    Copyright 2011  Martin Heide <martin.heide@gmx.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef IBUDDYFINDER_H
#define IBUDDYFINDER_H

#include "interfacesexport.h"
#include <QObject>
#include <KUrl>
#include "idocument.h"

namespace KDevelop {

/**
 * A plugin extension interface that you may implement in language plugins.
 * It enables the DocumentController to find related documents
 * (normally declaration/definition files, like foo.h and foo.cpp).
 * The DocumentController will position buddy tabs next to each other
 * if the option is enabled in the UIConfig.
 *
 * For finding a buddy document, the DocumentController addresses a language
 * plugin that treats the mimetype of the document.
 */
class KDEVPLATFORMINTERFACES_EXPORT IBuddyDocumentFinder
{
public:
    virtual ~IBuddyDocumentFinder() { }


    // TODO: Consider implementing a basic, language-independent version here,
    //       which regards files with the same basename as buddies

    
    /** \return a buddy document among the open documents,
      *         or 0 if there is none. */
    virtual IDocument* findBuddyDocument(const KUrl& url) = 0;
    virtual IDocument* findBuddyDocument(const IDocument* doc)
    {
        return findBuddyDocument(doc->url());
    }


    /** \return true, if the two documents are buddies. */
    virtual bool areBuddies(const KUrl& url1, const KUrl& url2) = 0;
    virtual bool areBuddies(const IDocument* doc1, const IDocument* doc2)
    {
        return areBuddies(doc1->url(), doc2->url());
    }


    /** \param two documents which are be buddies
      * \return true, if url1's tab should be placed left of url2's tab
      *         false, for the inverse */
    virtual bool buddyOrder(const KUrl& url1, const KUrl& url2) = 0;
    virtual bool buddyOrder(const IDocument* doc1, const IDocument* doc2)
    {
        return buddyOrder(doc1->url(), doc2->url());
    }
};

}

Q_DECLARE_INTERFACE( KDevelop::IBuddyDocumentFinder, "org.kdevelop.IBuddyDocumentFinder")

#endif
